<?php
    $cnpj = $_POST['cnpj'];
    $url = "https://www.receitaws.com.br/v1/cnpj/";

    //concatena o cnpj com o link para consulta
    $resultado = $url.$cnpj;
    
    $retorno = file_get_contents($resultado);
    $dadosJson = json_decode($retorno, TRUE);
    
    //echo $retorno;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css.css">
    <title>Consulta CNPJ</title>
</head>
<body>

<div class="container">


<?php 


if ($dadosJson['status'] != 'ERROR' && $dadosJson['status'] != 'OK' ) {
    echo "<div class='center'> <br><br><br><br><br>";     
    echo "<h2> Você excedeu o numero de tentativas, 
    é permitido somente 3 por minuto, 
    aguarde e tente novamente em breve!
    </div>
    <div>

    <br>
    <br>  
    <a href='index.php' class='btn btn-success center'>Nova Consulta</a></h2>";
    echo "</div>";
} 
else if ($dadosJson['status'] == 'ERROR') {
    echo "<div class='container1 center'>";     
    echo "<h2 class='box'>".$dadosJson['message']."<a href='index.php' class='btn btn-success center'>Nova Consulta</a><h2>";
    echo "</div>";
} 
else{
?>
        <br>
        <h2 class="text-center"><b>Dados da Empresa</b></h2>
        <hr>
        <form>
            <fieldset disabled>
            <div class="row">
                    <div class="col-md-6">
                        <label for="disabledTextInput">Razão Social</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['nome'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Nome Fantasia</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['fantasia'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Situação</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['situacao'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="disabledTextInput">Número do CNPJ</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['cnpj']?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Início das atividades</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['abertura'] ?>">
                    </div>
                    <div class="col-md-3">
                        <label for="disabledTextInput">Natureza Jurídica</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['natureza_juridica'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Tipo</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['tipo'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Capital Social</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo number_format($dadosJson['capital_social'], 2, ',', '.')?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="disabledTextInput">Logradouro</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['logradouro'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">Número</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['numero'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Complemento</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['complemento'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="disabledTextInput">Bairro</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['bairro'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">CEP</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['cep'] ?>">
                    </div>
                    <div class="col-md-4">
                        <label for="disabledTextInput">Município</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['municipio'] ?>">
                    </div>
                    <div class="col-md-2">
                        <label for="disabledTextInput">UF</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['uf'] ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label for="disabledTextInput">Telefone</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['telefone'] ?>">
                    </div>
                    <div class="col-md-6">
                        <label for="disabledTextInput">Email</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['email'] ?>">
                    </div>
                </div>
                <br>
                <h2 class="text-center"><b>Ramo de Atuação</b></h2><br>
                <h4><b>Atividade Econômica Principal</b></h4>
                <div class="row">
                    <div class="col-md-2">
                        <label for="disabledTextInput">Código</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['atividade_principal'][0]['code'] ?>">
                    </div>
                    <div class="col-md-10">
                        <label for="disabledTextInput">Descrição</label>
                        <input type="text" id="disabledTextInput" class="form-control" value="<?php echo $dadosJson['atividade_principal'][0]['text'] ?>">
                    </div>
                </div>
                <br>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Código</th>
                        <th scope="col">Atividade(s) Econômica(s) Secundaria(s)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dadosJson['atividades_secundarias'] as $atividades){
                            echo "<tr>";
                            echo "<th>".$atividades['code']."</th>";
                            echo "<td>".$atividades['text']."</td>";    
                            echo "</tr>";
                        }?>
                    </tbody>
                </table>
                <br>
                <h2 class="text-center"><b>Quadro de Sócios e Administradores</b></h2>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Qualificação</th>
                        <th scope="col">Nome</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($dadosJson['qsa'] as $socio){
                            echo "<tr>";
                            echo "<th>".$socio['qual']."</th>";
                            echo "<td>".$socio['nome']."</td>";    
                            echo "</tr>";
                        }?>
                    </tbody>
                </table>
            </fieldset>
        </form>

            

        <hr>
        <a href="index.php" class="btn btn-success">Nova Consulta</a><br><br><br>   
    </div>

    <?php }?>
</body>
</html>